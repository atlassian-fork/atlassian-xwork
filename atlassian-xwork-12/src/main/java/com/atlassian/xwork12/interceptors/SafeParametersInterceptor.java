package com.atlassian.xwork12.interceptors;

import com.atlassian.xwork12.Xwork12VersionSupport;

/**
 * //TODO define class
 */
public class SafeParametersInterceptor extends com.atlassian.xwork.interceptors.SafeParametersInterceptor {
    public SafeParametersInterceptor() {
        super(new Xwork12VersionSupport());
    }
}
