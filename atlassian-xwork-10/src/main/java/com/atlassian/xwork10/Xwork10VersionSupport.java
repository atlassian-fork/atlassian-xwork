package com.atlassian.xwork10;

import com.atlassian.xwork.XWorkVersionSupport;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionInvocation;

import java.lang.reflect.Method;

/**
 *
 */
public class Xwork10VersionSupport implements XWorkVersionSupport {
    public Action extractAction(ActionInvocation invocation) {
        return invocation.getAction();
    }

    public Method extractMethod(ActionInvocation invocation) throws NoSuchMethodException {
        return invocation.getProxy().getConfig().getMethod();
    }
}
