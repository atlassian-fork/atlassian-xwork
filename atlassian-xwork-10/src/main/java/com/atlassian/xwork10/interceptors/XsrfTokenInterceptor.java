package com.atlassian.xwork10.interceptors;

import com.atlassian.xwork10.Xwork10VersionSupport;

public class XsrfTokenInterceptor extends com.atlassian.xwork.interceptors.XsrfTokenInterceptor {
    public XsrfTokenInterceptor() {
        super(new Xwork10VersionSupport());
    }
}
