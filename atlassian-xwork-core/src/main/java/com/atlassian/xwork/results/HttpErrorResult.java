package com.atlassian.xwork.results;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.Result;

/**
 * XWork result that sends an HTTP error, as configured. Example usage:
 *
 * <blockquote><pre>&lt;result name="denied" type="httperror"&gt;
 *     &lt;param name="errorCode"&gt;403&lt;/param&gt;
 *     &lt;param name="errorMessage"&gt;You are not permitted to view this resource&lt;/param&gt;
 * &lt;/result&gt;</pre></blockquote>
 *
 * @since 1.6
 */
public class HttpErrorResult implements Result {
    private int errorCode;
    private String errorMessage;

    public void execute(ActionInvocation invocation) throws Exception {
        ServletActionContext.getResponse().sendError(errorCode, errorMessage);
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
