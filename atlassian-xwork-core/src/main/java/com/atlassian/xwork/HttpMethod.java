package com.atlassian.xwork;

import java.util.EnumSet;

/**
 * Enumeration of the different HTTP methods used by the {@link com.atlassian.xwork.interceptors.RestrictHttpMethodInterceptor}.
 * Because this enumeration is designed for the use of the interceptor (for example it treats HEAD as a sub-method
 * of GET), it probably isn't useful for more general cases.
 *
 * @since 1.6
 */
public enum HttpMethod {
    // Standard HTTP Methods

    /**
     * RFC2616 GET method. Will also match HEAD, as HEAD should be accepted anywhere GET is
     */
    GET {
        @Override
        public boolean matches(String methodName) {
            return super.matches(methodName) || HEAD.matches(methodName);
        }
    },
    /**
     * RFC2616 POST method
     */
    POST,
    /**
     * RFC2616 PUT method
     */
    PUT,
    /**
     * RFC2616 DELETE method
     */
    DELETE,
    /**
     * RFC2616 HEAD method
     */
    HEAD,
    /**
     * RFC2616 OPTIONS method
     */
    OPTIONS,
    /**
     * RFC2616 TRACE method
     */
    TRACE,
    /**
     * RFC2616 CONNECT method
     */
    CONNECT,

    // WEBDAV methods

    /**
     * RFC2518 PROPFIND method
     */
    PROPFIND,
    /**
     * RFC2518 PROPPATCH method
     */
    PROPPATCH,
    /**
     * RFC2518 MKCOL method
     */
    MKCOL,
    /**
     * RFC2518 COPY method
     */
    COPY,
    /**
     * RFC2518 MOVE method
     */
    MOVE,
    /**
     * RFC2518 LOCK method
     */
    LOCK,
    /**
     * RFC2518 UNLOCK method
     */
    UNLOCK,
    /**
     * RFC2518 PATCH method
     */
    PATCH,

    /**
     * Matches any method defined in standard HTTP/1.1
     */
    ALL_RFC2616 {
        @Override
        public boolean matches(String methodName) {
            for (HttpMethod method : EnumSet.range(GET, CONNECT)) {
                if (method.matches(methodName))
                    return true;
            }

            return false;
        }
    },

    /**
     * Matches any method defined in standard HTTP/1.1 or WEBDAV
     */
    ALL_DEFINED {
        @Override
        public boolean matches(String methodName) {
            for (HttpMethod method : EnumSet.range(GET, PATCH)) {
                if (method.matches(methodName))
                    return true;
            }

            return false;
        }
    },

    /**
     * Accept anything, even if it's not defined in this enumeration
     */
    ANY_METHOD {
        @Override
        public boolean matches(String methodName) {
            return true;
        }
    };

    /**
     * Checks if the given method name matches this HttpMethod.
     *
     * @param methodName the name. Can be null.
     * @return true if it anyMatch.
     */
    public boolean matches(String methodName) {
        return methodName != null && this.toString().equals(methodName);
    }

    /**
     * Checks if the given method name matches any of the given HttpMethods.
     *
     * @param methodName  the method name to check. Can be null.
     * @param httpMethods the methods to match against.
     * @return true if any of the given methods match.
     * @see #matches(String)
     * @since 2.1
     */
    public static boolean anyMatch(String methodName, HttpMethod... httpMethods) {
        for (HttpMethod allowedMethod : httpMethods) {
            if (allowedMethod.matches(methodName))
                return true;
        }

        return false;
    }
}
