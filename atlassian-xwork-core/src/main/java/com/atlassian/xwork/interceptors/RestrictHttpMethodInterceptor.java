package com.atlassian.xwork.interceptors;

import com.atlassian.xwork.HttpMethod;
import com.atlassian.xwork.PermittedMethods;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.Interceptor;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.joining;

/**
 * Interceptor used to restrict which HTTP methods are allowed to access which Action methods. Best used as a first
 * line of defence against XSRF attacks.
 *
 * <p>What HTTP methods are permitted may be configured either by adding the {@link com.atlassian.xwork.PermittedMethods}
 * annotation to the method that will be invoked on the action class, enumerating the methods that will be accepted, or
 * by adding a configuration parameter to the action definition in <code>xwork.xml</code>. If both are provided, the
 * <code>xwork.xml</code> configuration will be used, and any annotation-based configuration will be ignored. An example
 * of the parameter configuration:
 *
 * <blockquote><pre>&lt;action name="blah" class="com.example.MyAction"&gt;
 *     &lt;param name="permittedMethods"&gt;GET, POST, PUT&lt;/param&gt;
 *     &lt;result name="success" type="redirect"&gt;/index.html&lt;result&gt;
 * &lt;/action&gt;</pre></blockquote>
 *
 * <p>Note that method names are case sensitive, and all upper case. They must correspond to one of the values of the
 * {@link com.atlassian.xwork.HttpMethod} enum.</p>
 *
 * <p>Implementations should extend this class to configure a <code>SecurityLevel</code>. See the Javadoc of the
 * relevant class for what effect different security levels have on the operation of the interceptor.</p>
 *
 * <p>If the method execution is rejected, the interceptor returns an "invalidmethod" result. It is up to the
 * implementor to do something useful with that information.</p>
 *
 * @since 1.6
 */
public abstract class RestrictHttpMethodInterceptor implements Interceptor {
    private static final Logger log = Logger.getLogger(RestrictHttpMethodInterceptor.class);
    public static final String INVALID_METHOD_RESULT = "invalidmethod";
    public static final String PERMITTED_METHODS_PARAM_NAME = "permittedMethods";

    public enum SecurityLevel {
        /**
         * Do not restrict access at all.
         *
         * @deprecated since 2.1. Use {@link #DEFAULT} or {@link #STRICT} instead.
         */
        @Deprecated
        NONE {
            @Override
            public boolean isPermitted(String invocationMethodName, HttpMethod[] permittedMethods, String httpMethod) {
                return true;
            }
        },
        /**
         * Restrict access only on methods that are annotated.
         *
         * @deprecated since 2.1. Use {@link #DEFAULT} or {@link #STRICT} instead.
         */
        @Deprecated
        OPT_IN {
            @Override
            public boolean isPermitted(String invocationMethodName, HttpMethod[] permittedMethods, String httpMethod) {
                if (permittedMethods.length == 0)
                    return true;

                return HttpMethod.anyMatch(httpMethod, permittedMethods);
            }
        },
        /**
         * Restrict annotated methods as annotated. Allow GET and POST to un-annotated methods named doDefault().
         * Only allow POST to other methods.
         */
        DEFAULT {
            @Override
            public boolean isPermitted(String invocationMethodName, HttpMethod[] permittedMethods, String httpMethod) {
                if (permittedMethods.length == 0) {
                    if (invocationMethodName.equals("doDefault"))
                        return HttpMethod.anyMatch(httpMethod, HttpMethod.GET, HttpMethod.POST);
                    else
                        return HttpMethod.anyMatch(httpMethod, HttpMethod.POST);
                }

                return HttpMethod.anyMatch(httpMethod, permittedMethods);
            }
        },
        /**
         * Do not allow any invocation of methods that are not annotated
         */
        STRICT {
            @Override
            public boolean isPermitted(String invocationMethodName, HttpMethod[] permittedMethods, String httpMethod) {
                return HttpMethod.anyMatch(httpMethod, permittedMethods);
            }
        };

        public abstract boolean isPermitted(String invocationMethodName, HttpMethod[] permittedMethods, String httpMethod);
    }

    public final String intercept(ActionInvocation invocation) throws Exception {
        Method invocationMethod = invocation.getProxy().getConfig().getMethod();
        HttpMethod[] permittedMethods = getPermittedHttpMethods(invocation, invocationMethod);

        String httpMethod = getHttpMethod();

        if (log.isDebugEnabled())
            log.debug("Checking HTTP method: " + getHttpMethod() + " permitted against " + fullMethodName(invocationMethod));

        if (getSecurityLevel().isPermitted(invocationMethod.getName(), permittedMethods, httpMethod)) {
            log.debug("Invocation proceeding");
            return invocation.invoke();
        } else {
            log.info("Refusing HTTP method: " + httpMethod + " against " + fullMethodName(invocationMethod) + " (configured allowed methods: " + Arrays.toString(permittedMethods) + ")");
            // Note that this relies on integrators using a httpheader result to set a 405 status code
            final HttpServletResponse response = ServletActionContext.getResponse();
            if (response != null)
                response.setHeader("Allow", Arrays.stream(permittedMethods).map(Enum::toString).collect(joining(",")));
            return INVALID_METHOD_RESULT;
        }
    }

    /**
     * Collect all HTTP methods that are permitted for this invocation.
     * <p>
     * Subclasses can override this method to support any legacy annotations beyond {@link PermittedMethods}, and any
     * config params beyond {@code permittedMethods}. Subclasses are strongly encouraged to delegate to super within
     * their implementation to allow this default configuration to continue to work.
     *
     * @param invocation       the invocation.
     * @param invocationMethod the method to be invoked.
     * @return the {@link HttpMethod}s, or an empty array, if there are no annotations or config params present.
     * @since 2.1
     */
    protected HttpMethod[] getPermittedHttpMethods(ActionInvocation invocation, Method invocationMethod) {
        String configParam = (String) invocation.getProxy().getConfig().getParams().get(PERMITTED_METHODS_PARAM_NAME);
        PermittedMethods annotation = invocationMethod.getAnnotation(PermittedMethods.class);
        return toPermittedMethodArray(configParam, annotation);
    }

    private static HttpMethod[] toPermittedMethodArray(String configParam, PermittedMethods annotation) {
        if (configParam != null && configParam.trim().length() > 0) {
            String[] methodNames = configParam.trim().split("\\s*,\\s*");
            List<HttpMethod> permittedMethods = new ArrayList<HttpMethod>(methodNames.length);
            for (String methodName : methodNames) {
                try {
                    permittedMethods.add(HttpMethod.valueOf(methodName));
                } catch (IllegalArgumentException e) {
                    log.error("XWork configuration error: " + methodName + " is not a recognised HTTP method (method names are case sensitive).");
                }
            }

            return permittedMethods.toArray(new HttpMethod[0]);
        } else if (annotation != null) {
            return annotation.value();
        } else {
            return new HttpMethod[0];
        }
    }

    private String fullMethodName(Method invocationMethod) {
        return invocationMethod.getDeclaringClass().getName() + "#" + invocationMethod.getName();
    }

    private String getHttpMethod() {
        HttpServletRequest servletRequest = ServletActionContext.getRequest();
        return servletRequest == null ? "" : servletRequest.getMethod();
    }

    ///CLOVER:OFF
    public final void destroy() {
    }

    public final void init() {
    }

    /**
     * Get the currently configured security level for the interceptor. The default implementation will always return
     * SecurityLevel.DEFAULT. Implementors should override this method if they want to provide a mechanism for
     * configuring security levels.
     *
     * @return the currently configured security level for this interceptor
     */
    protected SecurityLevel getSecurityLevel() {
        return SecurityLevel.DEFAULT;
    }
}
