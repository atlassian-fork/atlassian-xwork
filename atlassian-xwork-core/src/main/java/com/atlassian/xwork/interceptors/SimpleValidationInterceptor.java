package com.atlassian.xwork.interceptors;

import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.Validateable;
import com.opensymphony.xwork.ValidationAware;
import com.opensymphony.xwork.interceptor.Interceptor;

/**
 * Very simple validation interceptor. If an action implements both Validateable
 * and ValidationAware, the interceptor will call the actions Validateable#validate() method,
 * and abort processing of the action (returning INPUT) if the validation results in errors.
 */
public class SimpleValidationInterceptor implements Interceptor {
    public String intercept(ActionInvocation invocation) throws Exception {
        Action action = invocation.getAction();
        if (action instanceof ValidationAware && action instanceof Validateable) {
            ((Validateable) action).validate();
            if (((ValidationAware) action).hasErrors()) {
                return Action.INPUT;
            }
        }

        return invocation.invoke();
    }

    ///CLOVER:OFF
    public void destroy() {
    }

    public void init() {
    }
}
