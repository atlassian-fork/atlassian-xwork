package com.atlassian.xwork;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Method level annotation for XWork actions to mark which HTTP Methods are permitted to be performed against
 * which action invocations.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PermittedMethods {
    HttpMethod[] value();
}
