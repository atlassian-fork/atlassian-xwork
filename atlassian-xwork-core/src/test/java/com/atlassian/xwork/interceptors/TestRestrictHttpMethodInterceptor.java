package com.atlassian.xwork.interceptors;

import com.atlassian.xwork.HttpMethod;
import com.atlassian.xwork.PermittedMethods;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.WebWorkStatics;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionProxy;
import com.opensymphony.xwork.ActionProxyFactory;
import com.opensymphony.xwork.ActionSupport;
import com.opensymphony.xwork.config.Configuration;
import com.opensymphony.xwork.config.ConfigurationException;
import com.opensymphony.xwork.config.ConfigurationManager;
import com.opensymphony.xwork.config.ConfigurationProvider;
import com.opensymphony.xwork.config.entities.ActionConfig;
import com.opensymphony.xwork.config.entities.PackageConfig;
import com.opensymphony.xwork.interceptor.Interceptor;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestRestrictHttpMethodInterceptor extends MockObjectTestCase {
    /*
        This is a reasonably complicated test because I wanted to ensure the interceptor works within the xwork
        framework (instead of mocking out how I expected the framework MIGHT work).
     */

    private RestrictHttpMethodInterceptor.SecurityLevel securityLevel;
    private Mock mockServletRequest;
    private Mock mockServletResponse;

    public static class UnAnnotatedAction extends ActionSupport {
        @Override
        public String execute() throws Exception {
            return SUCCESS;
        }

        @Override
        public String doDefault() throws Exception {
            return SUCCESS;

        }

        public String doEdit() throws Exception {
            return SUCCESS;
        }
    }

    public static class AnnotatedAction extends ActionSupport {
        @Override
        @PermittedMethods({HttpMethod.POST, HttpMethod.GET})
        public String execute() throws Exception {
            return SUCCESS;
        }

        @Override
        @PermittedMethods({HttpMethod.POST, HttpMethod.PUT})
        public String doDefault() throws Exception {
            return SUCCESS;

        }

        @PermittedMethods({HttpMethod.ANY_METHOD})
        public String anyMethod() throws Exception {
            return SUCCESS;
        }

        @PermittedMethods({HttpMethod.ALL_RFC2616})
        public String allRfc2616() throws Exception {
            return SUCCESS;
        }

        @PermittedMethods({HttpMethod.ALL_DEFINED})
        public String allDefined() throws Exception {
            return SUCCESS;
        }
    }

    private class SimpleInterceptor extends RestrictHttpMethodInterceptor {
        @Override
        protected SecurityLevel getSecurityLevel() {
            return securityLevel;
        }
    }

    private class CustomConfigurationProvider implements ConfigurationProvider {
        private List<Interceptor> interceptors;

        public void destroy() {
        }

        public void init(Configuration configuration) throws ConfigurationException {
            PackageConfig packageConfig = new PackageConfig();
            interceptors = new ArrayList<Interceptor>();
            interceptors.add(new SimpleInterceptor());

            // Note: XWork mapps a null method name to execute(), but config#getMethodName() still returns null so we
            // still test it separately just in case.

            packageConfig.addActionConfig("unannotated-execute", makeActionConfig(UnAnnotatedAction.class, "execute"));
            packageConfig.addActionConfig("unannotated-null", makeActionConfig(UnAnnotatedAction.class, null));
            packageConfig.addActionConfig("unannotated-dodefault", makeActionConfig(UnAnnotatedAction.class, "doDefault"));
            packageConfig.addActionConfig("unannotated-doedit", makeActionConfig(UnAnnotatedAction.class, "doEdit"));

            packageConfig.addActionConfig("configured-null", makeActionConfig(UnAnnotatedAction.class, null, "GET, POST"));
            packageConfig.addActionConfig("configured-execute", makeActionConfig(UnAnnotatedAction.class, "execute", "GET, POST"));
            packageConfig.addActionConfig("configured-dodefault", makeActionConfig(UnAnnotatedAction.class, "execute", "POST"));
            packageConfig.addActionConfig("configured-doedit", makeActionConfig(UnAnnotatedAction.class, "execute", "   GET,   PUT   , POST,CHEESE"));
            packageConfig.addActionConfig("configured-anymethod", makeActionConfig(UnAnnotatedAction.class, "execute", "ANY_METHOD"));
            packageConfig.addActionConfig("configured-rfc2616", makeActionConfig(UnAnnotatedAction.class, "execute", "ALL_RFC2616"));

            packageConfig.addActionConfig("annotated-execute", makeActionConfig(AnnotatedAction.class, "execute"));
            packageConfig.addActionConfig("annotated-null", makeActionConfig(AnnotatedAction.class, null));
            packageConfig.addActionConfig("annotated-dodefault", makeActionConfig(AnnotatedAction.class, "doDefault"));
            packageConfig.addActionConfig("annotated-anymethod", makeActionConfig(AnnotatedAction.class, "anyMethod"));
            packageConfig.addActionConfig("annotated-allrfc", makeActionConfig(AnnotatedAction.class, "allRfc2616"));
            packageConfig.addActionConfig("annotated-alldefined", makeActionConfig(AnnotatedAction.class, "allDefined"));

            configuration.addPackageConfig("defaultPackage", packageConfig);
        }

        private ActionConfig makeActionConfig(Class actionClass, String methodName, String permittedMethodsParameter) {
            return makeActionConfig(actionClass, methodName, Collections.singletonMap(RestrictHttpMethodInterceptor.PERMITTED_METHODS_PARAM_NAME, permittedMethodsParameter));
        }

        private ActionConfig makeActionConfig(Class actionClass, String methodName, Map parameterMap) {
            return new ActionConfig(methodName, actionClass, parameterMap, new HashMap(), interceptors);
        }

        private ActionConfig makeActionConfig(Class actionClass, String methodName) {
            return makeActionConfig(actionClass, methodName, new HashMap());
        }

        public boolean needsReload() {
            return false;
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        ConfigurationManager.addConfigurationProvider(new CustomConfigurationProvider());
        ConfigurationManager.getConfiguration().reload();

        mockServletRequest = new Mock(HttpServletRequest.class);
        mockServletResponse = new Mock(HttpServletResponse.class);
        ServletActionContext.setRequest((HttpServletRequest) mockServletRequest.proxy());
        ServletActionContext.setResponse((HttpServletResponse) mockServletResponse.proxy());
    }

    @Override
    protected void tearDown() throws Exception {
        ServletActionContext.setResponse(null);
        ServletActionContext.setRequest(null);
        ConfigurationManager.destroyConfiguration();
        ConfigurationManager.clearConfigurationProviders();
        super.tearDown();
    }

    public void testSecurityLevelNone() throws Exception {
        this.securityLevel = RestrictHttpMethodInterceptor.SecurityLevel.NONE;

        testInterceptor("unannotated-execute", "GET", Action.SUCCESS);
        testInterceptor("unannotated-execute", "POST", Action.SUCCESS);
        testInterceptor("unannotated-execute", "PROPFIND", Action.SUCCESS);
        testInterceptor("unannotated-execute", "MONKEY", Action.SUCCESS);

        testInterceptor("unannotated-null", "GET", Action.SUCCESS);
        testInterceptor("unannotated-null", "POST", Action.SUCCESS);
        testInterceptor("unannotated-null", "PROPFIND", Action.SUCCESS);
        testInterceptor("unannotated-null", "MONKEY", Action.SUCCESS);

        testInterceptor("unannotated-dodefault", "GET", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "POST", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "PROPFIND", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "MONKEY", Action.SUCCESS);

        testInterceptor("annotated-execute", "GET", Action.SUCCESS);
        testInterceptor("annotated-execute", "POST", Action.SUCCESS);
        testInterceptor("annotated-execute", "PROPFIND", Action.SUCCESS);
        testInterceptor("annotated-execute", "MONKEY", Action.SUCCESS);

        testInterceptor("annotated-null", "GET", Action.SUCCESS);
        testInterceptor("annotated-null", "POST", Action.SUCCESS);
        testInterceptor("annotated-null", "PROPFIND", Action.SUCCESS);
        testInterceptor("annotated-null", "MONKEY", Action.SUCCESS);

        testInterceptor("annotated-dodefault", "GET", Action.SUCCESS);
        testInterceptor("annotated-dodefault", "POST", Action.SUCCESS);
        testInterceptor("annotated-dodefault", "PROPFIND", Action.SUCCESS);
        testInterceptor("annotated-dodefault", "MONKEY", Action.SUCCESS);
    }

    public void testSecurityLevelOptIn() throws Exception {
        this.securityLevel = RestrictHttpMethodInterceptor.SecurityLevel.OPT_IN;

        testInterceptor("unannotated-execute", "GET", Action.SUCCESS);
        testInterceptor("unannotated-execute", "POST", Action.SUCCESS);
        testInterceptor("unannotated-execute", "PROPFIND", Action.SUCCESS);
        testInterceptor("unannotated-execute", "MONKEY", Action.SUCCESS);

        testInterceptor("unannotated-null", "GET", Action.SUCCESS);
        testInterceptor("unannotated-null", "POST", Action.SUCCESS);
        testInterceptor("unannotated-null", "PROPFIND", Action.SUCCESS);
        testInterceptor("unannotated-null", "MONKEY", Action.SUCCESS);

        testInterceptor("unannotated-dodefault", "GET", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "POST", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "PROPFIND", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "MONKEY", Action.SUCCESS);

        assertAnnotationsRespected();
    }

    public void testSecurityLevelDefault() throws Exception {
        this.securityLevel = RestrictHttpMethodInterceptor.SecurityLevel.DEFAULT;

        testInterceptor("unannotated-execute", "GET", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-execute", "POST", Action.SUCCESS);
        testInterceptor("unannotated-execute", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-execute", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("unannotated-null", "GET", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-null", "POST", Action.SUCCESS);
        testInterceptor("unannotated-null", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-null", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("unannotated-dodefault", "GET", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "HEAD", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "POST", Action.SUCCESS);
        testInterceptor("unannotated-dodefault", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-dodefault", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        assertAnnotationsRespected();
    }

    public void testNoContext() throws Exception {
        this.securityLevel = RestrictHttpMethodInterceptor.SecurityLevel.OPT_IN;

        testInterceptorWithNoContext("annotated-execute", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptorWithNoContext("unannotated-execute", Action.SUCCESS);
    }

    public void testSecurityLevelStrict() throws Exception {
        this.securityLevel = RestrictHttpMethodInterceptor.SecurityLevel.STRICT;

        testInterceptor("unannotated-execute", "GET", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-execute", "POST", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-execute", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-execute", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("unannotated-null", "GET", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-null", "POST", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-null", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-null", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("unannotated-dodefault", "GET", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-dodefault", "POST", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-dodefault", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("unannotated-dodefault", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        assertAnnotationsRespected();
    }

    private void assertAnnotationsRespected() throws Exception {
        testInterceptor("annotated-execute", "GET", Action.SUCCESS);
        testInterceptor("annotated-execute", "HEAD", Action.SUCCESS);
        testInterceptor("annotated-execute", "POST", Action.SUCCESS);
        testInterceptor("annotated-execute", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("annotated-execute", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("annotated-null", "GET", Action.SUCCESS);
        testInterceptor("annotated-null", "HEAD", Action.SUCCESS);
        testInterceptor("annotated-null", "POST", Action.SUCCESS);
        testInterceptor("annotated-null", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("annotated-null", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("annotated-dodefault", "GET", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("annotated-dodefault", "POST", Action.SUCCESS);
        testInterceptor("annotated-dodefault", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("annotated-dodefault", "PUT", Action.SUCCESS);

        testInterceptor("annotated-allrfc", "GET", Action.SUCCESS);
        testInterceptor("annotated-allrfc", "HEAD", Action.SUCCESS);
        testInterceptor("annotated-allrfc", "POST", Action.SUCCESS);
        testInterceptor("annotated-allrfc", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("annotated-allrfc", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("annotated-anymethod", "GET", Action.SUCCESS);
        testInterceptor("annotated-anymethod", "HEAD", Action.SUCCESS);
        testInterceptor("annotated-anymethod", "POST", Action.SUCCESS);
        testInterceptor("annotated-anymethod", "PROPFIND", Action.SUCCESS);
        testInterceptor("annotated-anymethod", "MONKEY", Action.SUCCESS);

        testInterceptor("annotated-alldefined", "GET", Action.SUCCESS);
        testInterceptor("annotated-alldefined", "PROPFIND", Action.SUCCESS);
        testInterceptor("annotated-alldefined", "CHEESE", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("configured-execute", "GET", Action.SUCCESS);
        testInterceptor("configured-execute", "HEAD", Action.SUCCESS);
        testInterceptor("configured-execute", "POST", Action.SUCCESS);
        testInterceptor("configured-execute", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("configured-execute", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("configured-null", "GET", Action.SUCCESS);
        testInterceptor("configured-null", "HEAD", Action.SUCCESS);
        testInterceptor("configured-null", "POST", Action.SUCCESS);
        testInterceptor("configured-null", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("configured-null", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("configured-dodefault", "GET", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("configured-dodefault", "POST", Action.SUCCESS);
        testInterceptor("configured-dodefault", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("configured-dodefault", "MONKEY", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("configured-doedit", "GET", Action.SUCCESS);
        testInterceptor("configured-doedit", "HEAD", Action.SUCCESS);
        testInterceptor("configured-doedit", "PUT", Action.SUCCESS);
        testInterceptor("configured-doedit", "POST", Action.SUCCESS);
        testInterceptor("configured-doedit", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        // Not really a method, so it's not even allowed to be configured.
        testInterceptor("configured-doedit", "CHEESE", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);

        testInterceptor("configured-anymethod", "GET", Action.SUCCESS);
        testInterceptor("configured-anymethod", "PROPFIND", Action.SUCCESS);
        testInterceptor("configured-anymethod", "CHEESE", Action.SUCCESS);

        testInterceptor("configured-rfc2616", "GET", Action.SUCCESS);
        testInterceptor("configured-rfc2616", "PROPFIND", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
        testInterceptor("configured-rfc2616", "CHEESE", RestrictHttpMethodInterceptor.INVALID_METHOD_RESULT);
    }

    private void testInterceptor(String actionAlias, String httpMethod, String expectedResult) throws Exception {
        mockServletResponse.expects(atMostOnce()).method("setHeader").withAnyArguments();
        ActionProxy proxy = ActionProxyFactory.getFactory().createActionProxy("", actionAlias, makeContext(), false);
        matchMethod(httpMethod);
        assertEquals("Testing " + httpMethod + " against " + actionAlias, expectedResult, proxy.execute());
    }

    private void testInterceptorWithNoContext(String actionAlias, String expectedResult) throws Exception {
        ActionProxy proxy = ActionProxyFactory.getFactory().createActionProxy("", actionAlias, new HashMap(), false);
        assertEquals("Testing no request against " + actionAlias, expectedResult, proxy.execute());
    }

    private HashMap makeContext() {
        HashMap<String, Object> context = new HashMap<String, Object>();
        context.put(WebWorkStatics.HTTP_REQUEST, mockServletRequest.proxy());
        context.put(WebWorkStatics.HTTP_RESPONSE, mockServletResponse.proxy());
        return context;
    }

    private void matchMethod(String httpMethodName) {
        mockServletRequest.stubs().method("getMethod").withNoArguments().will(returnValue(httpMethodName));
    }
}
